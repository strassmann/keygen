import java.sql.Date;

public class Key {
    public String key;

    public Date keyValidUntil;

    public Boolean redeemed;

    public Long durationDays;
    public Date permissionUntil;

    public String comment;

    public Boolean productSystembrett;
    public Boolean productInneresteam;
    public Boolean productMoodmap;
    public Boolean productWhiteboard;
    public Boolean productEtherpad;
    public Boolean productCardSelection3d;
    public Boolean productVideoConference;
    public String corporateCustomerId;

    Key(String key, Date keyValidUntil, boolean redeemed, String comment, Long durationDays, Date permissionUntil, Boolean productSystembrett,
            boolean productInneresteam, Boolean productMoodmap, Boolean productWhiteboard, Boolean productEtherpad,
            Boolean productCardSelection3d, Boolean productVideoConference, String corporateCustomerId
        ) {
        this.key = key;
        this.redeemed = redeemed;
        this.comment = comment;
        this.durationDays = durationDays;
        this.permissionUntil = permissionUntil;
        this.productSystembrett = productSystembrett;
        this.productInneresteam = productInneresteam;
        this.productMoodmap = productMoodmap;
        this.productWhiteboard = productWhiteboard;
        this.productEtherpad = productEtherpad;
        this.keyValidUntil = keyValidUntil;
        this.productCardSelection3d = productCardSelection3d;
        this.productVideoConference = productVideoConference;
        this.corporateCustomerId = corporateCustomerId;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Key)) {
            return false;
        }
        Key key = (Key) o;
        if (key.key == this.key) {
            return true;
        } else {
            return false;
        }
    }

}
