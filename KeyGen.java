import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class KeyGen {

    public static void main(String args[]) {

        // In VSCODE ausführen mit 
        // cmd /C "d:\Programme\openjdk-20\jdk-20.0.1\bin\java.exe -XX:+ShowCodeDetailsInExceptionMessages -cp C:\Users\soeren\AppData\Roaming\Code\User\workspaceStorage\8893085d77637c57bf1f39fed7b3c467\redhat.java\jdt_ws\keygen_99192126\bin KeyGen "

        // Output
        // SQL commands --> sql.txt
        // Key database --> keys.txt
        // Keys from this generation cycle --> filename defined here:
        String fileName = "key-formatted";
        // String server = "test";
        String server = "prod";

        // Input Parameter
        int keyLength = 9; // 3, 6 , 9
        int numberOfKeys = 1;

        Long durationDays = null; // OR
        
        Date permissionUntil = Date.valueOf(LocalDate.of(2025, 9, 30));

        Date validUntil = Date.valueOf(LocalDate.of(2026, 12, 30));
        String comment = "1x Key für Lea Menzel - ISTOB Ausbildung";
        Boolean productSystembrett = true;
        Boolean productInneresteam = true;
        Boolean productMoodmap = true;
        Boolean productWhiteboard = true; // should be true
        Boolean productEtherpad = true; // should be true
        Boolean productCardSelection3d = true;
        Boolean productVideoConference = false;
        // Optional
        String corporateCustomerId = null;

        if (checkInputValidity(durationDays, permissionUntil)) {

            // Load old
            ArrayList<String> keyDatabase = loadKeyDatabase(server);

            // Generate Keys
            List<Key> newKeyList = new ArrayList<Key>();
            for (int i = 0; i < numberOfKeys; i++) {
                String newKeyString = createKey(keyLength);
                Key newKey = new Key(newKeyString, validUntil, false, comment, durationDays, permissionUntil, productSystembrett,
                        productInneresteam, productMoodmap, productWhiteboard, productEtherpad,
                        productCardSelection3d, productVideoConference, corporateCustomerId);
                newKeyList.add(newKey);
            }

            // Convert to string list
            ArrayList<String> newKeyListString = new ArrayList<String>();
            for (Key key : newKeyList) {
                newKeyListString.add(key.key);
            }

            boolean foundKey = false;
            for (String string : keyDatabase) {
                for (String string2 : newKeyListString) {
                    if (string.equals((string2))) {
                        foundKey = true;
                        System.out.println("Duplicate Key: " + string2);
                    }
                }
            }
            for (String string : newKeyListString) {
                int counter = 0;
                for (String string2 : newKeyListString) {
                    if (string.equals((string2))) {
                        counter++;
                    }
                }
                if (counter >= 2) {
                    System.out.println("Duplicate Key in generated Set: " + string);
                }
            }

            if (!foundKey) {
                makeSQL(server, fileName, newKeyList);
                writeToKeyDataStore(newKeyList, server);
                writeToSeparateKeyList(server, fileName, newKeyList);
                writeCleanCSV(server, fileName, newKeyList);
            } else {
                System.out.println("Wrote nothing to keystore");
            }

        } else {
            System.out.println("Input errors for the new key");
        }
    }

    private static String createKey(int keyLength) {
        String key = "";

        for (int a = 1; a <= keyLength; a++) {
            key = key + getRandomCharacter();

            if (a % 3 == 0 && a < keyLength) {
                key = key + "-";
            }
        }
        return key;
    }

    private static char getRandomCharacter() {
        String alphabet = "0123456789abcdefghijklmnopqrstuvwxyz".toUpperCase();
        double random = Math.random() * alphabet.length();
        int index = (int) random;
        return alphabet.toCharArray()[index];
    }

    private static void makeSQL(String server, String fileName, List<Key> keys) {
        try {
            File file = new File(server + "-" + fileName + "-sql.txt");

            if (!file.exists()) {
                file.createNewFile();
            }

            BufferedWriter myWriter = new BufferedWriter(new FileWriter(file, true));
            myWriter.write(keys.get(0).comment + " / generated on: " + LocalDate.now());
            myWriter.newLine();

            for (Key key : keys) {
                myWriter.write(
                        "INSERT INTO product_key (key, key_valid_until, redeemed, comment, duration_days, permission_until, product_systembrett, product_inneresteam, product_moodmap, product_whiteboard, product_etherpad, product_card_selection3d, product_video_conference, corporate_customer_id, created_date, modified_date)"
                                + " VALUES ('" + key.key + "', '" 
                                + key.keyValidUntil + "', " 
                                + key.redeemed + ", '"
                                + key.comment + "', " 
                                + key.durationDays + ", " 
                                + getPermissionUntilSqlString(key.permissionUntil) + ", "
                                + key.productSystembrett + ", "
                                + key.productInneresteam + ", " 
                                + key.productMoodmap + ", " 
                                + key.productWhiteboard + ", " 
                                + key.productEtherpad + ", " 
                                + key.productCardSelection3d + ", " 
                                + key.productVideoConference + ", " 
                                + getCorporateCustomerIdSqlString(key.corporateCustomerId) + ", "
                                + "current_timestamp" + ", "
                                + "current_timestamp" + ");");
                myWriter.newLine();
            }

            myWriter.newLine();
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private static String getPermissionUntilSqlString(Date permissionUntil) {
        if (permissionUntil != null) {
            return "'" + permissionUntil + "'";
        }
        return "null";
    }

    private static String getCorporateCustomerIdSqlString(String corporateCustomerId) {
        if (corporateCustomerId != null) {
            return "'" + corporateCustomerId + "'";
        }
        return "null";
    }

    private static void writeToKeyDataStore(List<Key> keys, String server) {
        try {
            File file = new File(server + "-key-store.txt");
            BufferedWriter myWriter = new BufferedWriter(new FileWriter(file, true));
            if (!file.exists()) {
                file.createNewFile();
            }

            for (Key key : keys) {
                myWriter.write(key.key);
                myWriter.newLine();
            }

            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private static ArrayList<String> loadKeyDatabase(String server) {
        ArrayList<String> keyDatabase = new ArrayList<String>();
        try {
            File file = new File(server + "-key-store.txt");

            if (!file.exists()) {
                file.createNewFile();
            }

            BufferedReader myReader = new BufferedReader(new FileReader(file));

            String line = myReader.readLine();
            while (line != null) {
                keyDatabase.add(line);
                line = myReader.readLine();
            }

            myReader.close();

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return keyDatabase;
    }

    private static void writeToSeparateKeyList(String server, String fileName, List<Key> keys) {
        try {
            File file = new File(server + "-" + fileName + ".txt");

            if (!file.exists()) {
                file.createNewFile();
            }

            BufferedWriter myWriter = new BufferedWriter(new FileWriter(file, true));
            myWriter.write(keys.get(0).comment + " / generated on: " + LocalDate.now());
            myWriter.newLine();

            myWriter.write(String.format("%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s", "key", "redeemed",
                    "durationDays", "permissionUntil", "keyValidUntil", "productSystembrett", "productInneresteam",
                    "productMoodmap", "productCardSelection3d", "productWhiteboard", "productEtherpad", "productVideoConference",
                    "corporateCustomerId"));
            myWriter.newLine();

            for (Key key : keys) {
                myWriter.write(String.format("%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s", key.key, key.redeemed,
                        key.durationDays, key.permissionUntil, key.keyValidUntil, key.productSystembrett, key.productInneresteam,
                        key.productMoodmap, key.productCardSelection3d, key.productWhiteboard, key.productEtherpad, key.productVideoConference, key.corporateCustomerId));
                myWriter.newLine();
            }
            myWriter.newLine();

            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private static void writeCleanCSV(String server, String fileName, List<Key> keys) {
        try {
            File file = new File(server + "-" + fileName + "-csv.csv");
            BufferedWriter myWriter = new BufferedWriter(new FileWriter(file, true));
            if (!file.exists()) {
                file.createNewFile();
            }

            for (Key key : keys) {
                myWriter.write(key.key);
                myWriter.newLine();
            }

            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private static Boolean checkInputValidity(Long durationDays, Date permissionUntil) {
        if (durationDays == null && permissionUntil == null) {
            return false;
        } else if (durationDays != null && permissionUntil != null) {
            return false;
        } else {
            return true;
        }
    }

}
